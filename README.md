An example of taking pixel brightness from a video and manipulating individual objects with it in Unity, demonstrated here by Bad Apple (https://www.youtube.com/watch?v=FtutLA63Cp8).

Build is in the build folder, will probably separate later.

To run it, run Bad Apple.exe in the Build folder.

Built in Unity 2021.1.11f, so expect it to work in that version.

Press spacebar to play/pause the video, press ESC to open menu.

**BUILD INSTRUCTIONS:**
Open the project in Unity and select File -> Build and Run.
