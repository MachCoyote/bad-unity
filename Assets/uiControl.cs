using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiControl : MonoBehaviour
{
    float count;
    float prefps;
    public string fps;
    public Text fpstext;
    public Canvas canvas;
    public Canvas fpscanvas;
    public Toggle fpstoggle;
    public InputField whiteText;
    public InputField blackText;
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 0;
        canvas.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (fpstoggle.isOn)
        {
            fpscanvas.gameObject.SetActive(true);
        }
        else
        {
            fpscanvas.gameObject.SetActive(false);
        }

        if (Time.timeScale == 1)
        {
            count = 1 / Time.deltaTime;
            prefps = Mathf.Round(count);
            fps = prefps.ToString();
        }
        fpstext.text = fps;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (canvas.gameObject.activeInHierarchy)
            {
                canvas.gameObject.SetActive(false);
            }
            else
            {
                canvas.gameObject.SetActive(true);
            }
        }
    }

    public void exit()
    {
        Application.Quit();
    }
}
