using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour
{
    public Transform camTransform;

    // Start is called before the first frame update
    void Start()
    {
        camTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            camTransform.Translate(new Vector3(0, 0, 100 * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.A))
        {
            camTransform.Translate(new Vector3(-100 * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            camTransform.Translate(new Vector3(0, 0, -100 * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.D))
        {
            camTransform.Translate(new Vector3(100 * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            camTransform.Translate(new Vector3(0, 100 * Time.deltaTime, 0), Space.World);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            camTransform.Translate(new Vector3(0, -100 * Time.deltaTime, 0), Space.World);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            camTransform.Rotate(new Vector3(-100 * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            camTransform.Rotate(new Vector3(0, -100 * Time.deltaTime, 0), Space.World);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            camTransform.Rotate(new Vector3(100 * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            camTransform.Rotate(new Vector3(0, 100 * Time.deltaTime, 0), Space.World);
        }
    }
}
