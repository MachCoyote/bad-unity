using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class badApple : MonoBehaviour
{
    GameObject currentCube;
    public RenderTexture render;
    Texture2D currentFrame;
    public Material testMat;
    public float depth = 5;
    public GameObject cube;
    int gen = 1;
    public Material black;
    public Material white;
    public Material cubeMaterial;
    public uiControl ui;
    public VideoPlayer video;
    
    // Start is called before the first frame update
    void Start()
    {
        currentFrame = new Texture2D(render.width, render.height);
        for (int i = 1; i < render.width; i++)
        {
            for (int o = 1; o < render.height; o++)
            {
                Vector3 pos = new Vector3(i, o, depth);
                GameObject testCube = Instantiate(cube, pos, Quaternion.Euler(0, 0, 0));
                testCube.name = gen + "";
                gen++;
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            physicsEnable();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (video.isPlaying)
                video.Pause();
            else
                video.Play();
        }

        RenderTexture.active = render;
        currentFrame.ReadPixels(new Rect(0, 0, render.width, render.height), 0, 0, false);
        currentFrame.Apply();
        print(currentFrame.GetPixel(1, 1).grayscale);
        testMat.mainTexture = currentFrame;
        badAppleMeth();
    }
    
    void badAppleMeth()
    {
        int curGen = 1;
        for (int i = 1; i < render.width; i++)
        {
            for (int o = 1; o < render.height; o++)
            {
                currentCube = GameObject.Find(curGen.ToString());

                if (currentFrame.GetPixel(i, o).grayscale == 0)
                {
                    currentCube.GetComponent<Renderer>().material = black;
                    currentCube.GetComponent<Transform>().position = new Vector3(currentCube.GetComponent<Transform>().position.x, currentCube.GetComponent<Transform>().position.y, int.Parse(ui.blackText.text));
                }
                else if (currentFrame.GetPixel(i, o).grayscale > 0.94)
                {
                    currentCube.GetComponent<Renderer>().material = white;
                    currentCube.GetComponent<Transform>().position = new Vector3(currentCube.GetComponent<Transform>().position.x, currentCube.GetComponent<Transform>().position.y, int.Parse(ui.whiteText.text));
                }
                else if (currentFrame.GetPixel(i, o).grayscale < 0.95 & currentFrame.GetPixel(i, o).grayscale > 0)
                {
                    currentCube.GetComponent<Renderer>().material = black;
                    currentCube.GetComponent<Transform>().position = new Vector3(currentCube.GetComponent<Transform>().position.x, currentCube.GetComponent<Transform>().position.y, int.Parse(ui.blackText.text));
                }
                
                curGen++;
                
            }
        }
    }

    void physicsEnable()
    {
        int currGen = 1;
        for (int i = 1; i < render.width; i++)
        {
            for (int o = 1; o < render.height; o++)
            {
                currentCube = GameObject.Find(currGen.ToString());

                currentCube.GetComponent<Rigidbody>().isKinematic = false;
                currentCube.GetComponent<BoxCollider>().enabled = true;
                currGen++;

            }
        }
    }
}
